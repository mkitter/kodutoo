package baccarat;

import javafx.scene.control.TextField;

public class Meetodid {

	// Meetod, mis v6tab String sisestuse ja kontrollib selle vastavust
	// aktsepteeritavate s6nade suhtes.
	public static String panus(String sisestus) {
		if ((sisestus.toLowerCase().equals("50")) || (sisestus.toLowerCase().equals("100"))
				|| (sisestus.toLowerCase().equals("200")) || (sisestus.toLowerCase().equals("500"))
				|| (sisestus.toLowerCase().equals("007"))) {
			return sisestus;
		} else {
			return "Vale sisestus";
		}
	}

	// Meetod, mis v6tab String sisestuse ja kontrollib selle vastavust
	// aktsepteeritavate s6nade suhtes.
	public static String ennustus(String sisestus) {
		if ((sisestus.toLowerCase().equals("m�ngija")) || (sisestus.toLowerCase().equals("pank"))
				|| (sisestus.toLowerCase().equals("viik")) || (sisestus.toLowerCase().equals("007"))) {
			return sisestus;
		} else {
			return "Vale sisestus";
		}

	}

	// Meetod, mis tagastab arvu viimase numbri.
	public static int viimaneNumber(int number) {
		return number % 10;
	}

	// Meetod summa arvutamiseks.
	public static int summa(int a, int b) {
		return a + b;
	}

	// Meetod, mis v6tab kasutajaliidesesse pandud teksti ja muudab selle String
	// t66pi v22rtuseks.
	public static String stringSisestus(TextField sisestamine) {
		String sisestus = sisestamine.getText();
		return sisestus.toLowerCase();
	}
}

package baccarat;

public class Kaardipakk {

	// Meetod, mis annab massiivi kaartidega.
	public static String[] kaardid() {
		String[] kaardid = { "ruutu 2", "ruutu 3", "ruutu 4", "ruutu 5", "ruutu 6", "ruutu 7", "ruutu 8", "ruutu 9",
				"ruutu 10", "ruutu soldat", "ruutu emand", "ruutu kuningas", "ruutu �ss", "risti 2", "risti 3",
				"risti 4", "risti 5", "risti 6", "risti 7", "risti 8", "risti 9", "risti 10", "risti soldat",
				"risti emand", "risti kuningas", "risti �ss", "�rtu 2", "�rtu 3", "�rtu 4", "�rtu 5", "�rtu 6",
				"�rtu 7", "�rtu 8", "�rtu 9", "�rtu 10", "�rtu soldat", "�rtu emand", "�rtu kuningas", "�rtu �ss",
				"poti 2", "poti 3", "poti 4", "poti 5", "poti 6", "poti 7", "poti 8", "poti 9", "poti 10",
				"poti soldat", "poti emand", "poti kuningas", "poti �ss" };

		return kaardid;

	}

	// Meetod, mis annab massiivist �he elemendi.
	public static String suvaKaart(String[] kaardid) {
		int juhuslik = (int) (Math.random() * kaardid.length);
		return kaardid[juhuslik];
	}

}

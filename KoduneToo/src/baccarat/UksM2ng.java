package baccarat;

public class UksM2ng {

	// Meetod, kuhu sisestakse int v22rtusega raha ning panuse ja String
	// ennustuse p6hjal tuleb v2lja massiiv, mis sisaldab m2ngu tulemusi.
	public static String[] m2ng(int raha, int panus, String ennustus) {
		while (true) {
			String tulemus;

			// Leitakse m2ngija ja panga v22rtused kasutades KaartideSaamine
			// klassis olevaid meetodeid.
			String[] m2ngijaTulemus = KaartideSaamine.m2ngijaKaardid();
			int m2ngijaV22rtus = Integer.parseInt(m2ngijaTulemus[0]);
			int m2ngijaK2si = Integer.parseInt(m2ngijaTulemus[1]);
			String m2ngijaKaardid = m2ngijaTulemus[2];
			String[] pangaTulemus = KaartideSaamine.pangaKaardid(m2ngijaK2si);
			int pangaV22rtus = Integer.parseInt(pangaTulemus[0]);
			String pangaKaardid = pangaTulemus[1];

			// Tulemuse leidmine.
			if (m2ngijaV22rtus > pangaV22rtus) {
				tulemus = "m�ngija";
			} else if (pangaV22rtus > m2ngijaV22rtus) {
				tulemus = "pank";
			} else {
				tulemus = "viik";
			}

			// Ennustuse ja tulemuse v6rdlemine ning rahaseisu tagaj2rg.
			if ((ennustus.toLowerCase().equals(tulemus)) && (tulemus.equals("viik"))) {
				raha = raha + (panus * 7);
			} else if (ennustus.toLowerCase().equals(tulemus)) {
				raha = raha + panus;
			} else {
				raha = raha - panus;
			}

			// Massiiv, mis sisaldab raha, m2ngija kaartide v22rtust, panga
			// kaartide v22rtust, m2ngija poolt tehtud ennustust, ning m2ngu
			// tulemust.
			String[] vastus = { ennustus, Integer.toString(m2ngijaV22rtus), Integer.toString(pangaV22rtus), tulemus,
					Integer.toString(raha), m2ngijaKaardid, pangaKaardid };

			return vastus;

		}

	}

}

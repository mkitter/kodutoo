package baccarat;

import java.util.ArrayList;
import java.util.Arrays;

public class KaartideSaamine {

	// Meetod, kus m2ngija saab oma kaardid. Meetod tagastab m2ngija kaartide
	// tulemused, kaartide arvu ning m2ngija kaardid.
	public static String[] m2ngijaKaardid() {
		int m2ngijaV22rtus;
		int m2ngijaViimaneV22rtus;
		int m2ngijaK2si;
		ArrayList<String> kaardid = new ArrayList<String>();

		// M2ngija esimese kaardi saamine.
		String m2ngijaEsimeneKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
		kaardid.add(m2ngijaEsimeneKaart);
		int m2ngijaEsimeseKaardiV22rtus = IntiksMuut.konverter(m2ngijaEsimeneKaart);
		System.out.println("M�ngija esimene kaart: " + m2ngijaEsimeneKaart);

		if (m2ngijaEsimeseKaardiV22rtus > 7) {
			m2ngijaViimaneV22rtus = m2ngijaEsimeseKaardiV22rtus;
			m2ngijaK2si = 1;
			// M2ngija teise kaardi saamise tingimus.
		} else {
			String m2ngijaTeineKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
			kaardid.add(m2ngijaTeineKaart);
			System.out.println("M�ngija teine kaart: " + m2ngijaTeineKaart);
			int m2ngijaTeiseKaardiV22rtus = IntiksMuut.konverter(m2ngijaTeineKaart);
			int kaheKaardiSumma = Meetodid.summa(m2ngijaEsimeseKaardiV22rtus, m2ngijaTeiseKaardiV22rtus);
			m2ngijaV22rtus = Meetodid.viimaneNumber(kaheKaardiSumma);
			if (m2ngijaV22rtus > 5) {
				m2ngijaViimaneV22rtus = m2ngijaV22rtus;
				m2ngijaK2si = 2;
				// M2ngija kolmanda kaardi saamise tingimus.
			} else {
				String m2ngijaKolmasKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
				kaardid.add(m2ngijaKolmasKaart);
				System.out.println("M�ngija kolmas kaart: " + m2ngijaKolmasKaart);
				int m2ngijaKolmandaKaardiV22rtus = IntiksMuut.konverter(m2ngijaKolmasKaart);
				int kolmeKaardiSumma = Meetodid.summa(m2ngijaV22rtus, m2ngijaKolmandaKaardiV22rtus);
				m2ngijaViimaneV22rtus = Meetodid.viimaneNumber(kolmeKaardiSumma);
				m2ngijaK2si = 3;

			}

		}
		String[] kaardid2 = new String[kaardid.size()];
		kaardid2 = kaardid.toArray(kaardid2);
		String[] tulemus = { Integer.toString(m2ngijaViimaneV22rtus), Integer.toString(m2ngijaK2si),
				Arrays.toString(kaardid2) };
		return tulemus;
	}

	// Meetod, kus pank saab oma kaardid. V�tab muutuja m2ngijak2si ning
	// tagastab panga k2e v22rtuse ning panga kaardid.
	public static String[] pangaKaardid(int m2ngijaK2si) {
		int pangaV22rtus;
		int pangaViimaneV22rtus;
		ArrayList<String> kaardid = new ArrayList<String>();

		// Panga esimese kaardi saamine.
		String pangaEsimeneKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
		kaardid.add(pangaEsimeneKaart);
		int pangaEsimeseKaardiV22rtus = IntiksMuut.konverter(pangaEsimeneKaart);
		System.out.println("Panga esimene kaart: " + pangaEsimeneKaart);

		if (pangaEsimeseKaardiV22rtus > 7) {
			pangaViimaneV22rtus = pangaEsimeseKaardiV22rtus;
			// Panga teise kaardi saamise tingimus.
		} else {
			String pangaTeineKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
			kaardid.add(pangaTeineKaart);
			System.out.println("Panga teine kaart: " + pangaTeineKaart);
			int pangaTeiseKaardiV22rtus = IntiksMuut.konverter(pangaTeineKaart);
			int kaheKaardiSumma2 = Meetodid.summa(pangaEsimeseKaardiV22rtus, pangaTeiseKaardiV22rtus);
			pangaV22rtus = Meetodid.viimaneNumber(kaheKaardiSumma2);
			if ((m2ngijaK2si < 3) && (pangaV22rtus > 5)) {
				pangaViimaneV22rtus = pangaV22rtus;
				// Panga kolmanda kaardi saamise tingimus.
			} else if ((m2ngijaK2si == 3) && (pangaV22rtus < 7)) {
				String pangaKolmasKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
				kaardid.add(pangaKolmasKaart);
				System.out.println("Panga kolmas kaart: " + pangaKolmasKaart);
				int pangaKolmandaKaardiV22rtus = IntiksMuut.konverter(pangaKolmasKaart);
				int kolmeKaardiSumma2 = Meetodid.summa(pangaV22rtus, pangaKolmandaKaardiV22rtus);
				pangaViimaneV22rtus = Meetodid.viimaneNumber(kolmeKaardiSumma2);
			} else if ((m2ngijaK2si == 3) && (pangaV22rtus > 6)) {
				pangaViimaneV22rtus = pangaV22rtus;
			} else {
				String pangaKolmasKaart = Kaardipakk.suvaKaart(Kaardipakk.kaardid());
				kaardid.add(pangaKolmasKaart);
				System.out.println("Panga kolmas kaart: " + pangaKolmasKaart);
				int pangaKolmandaKaardiV22rtus = IntiksMuut.konverter(pangaKolmasKaart);
				int kolmeKaardiSumma2 = Meetodid.summa(pangaV22rtus, pangaKolmandaKaardiV22rtus);
				pangaViimaneV22rtus = Meetodid.viimaneNumber(kolmeKaardiSumma2);
			}
		}
		String[] kaardid2 = new String[kaardid.size()];
		kaardid2 = kaardid.toArray(kaardid2);
		String[] tulemus = { Integer.toString(pangaViimaneV22rtus), Arrays.toString(kaardid2) };
		return tulemus;
	}

}

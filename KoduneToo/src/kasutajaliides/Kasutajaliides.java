package kasutajaliides;

import baccarat.Meetodid;
import baccarat.UksM2ng;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;

public class Kasutajaliides extends Application {

	Button nupp1;
	Button nupp2;
	Button nupp3;
	Button nupp4;
	Button nupp5;
	Button nupp6;
	Stage aken;
	Scene stseen1;
	Scene stseen2;
	Scene stseen3;
	Scene stseen4;
	String[] tulemused;
	int raha = 500;

	@Override
	public void start(Stage primaryStage) {

		aken = primaryStage;
		aken.setTitle("Baccarat");

		// K6ik kasutatud sildid, label1 v22rtus muutub m2ngu m2ngides.
		Label label1 = new Label("Raha j��k: " + Integer.toString(raha));
		Label label2 = new Label("Sisesta panus (50, 100, 200, 500):");
		Label label3 = new Label("Sisesta, kes m�ngu v�idab (m�ngija, pank, viik):");
		Label labelTulemus1 = new Label();
		Label labelTulemus2 = new Label();
		Label labelTulemus3 = new Label();
		Label labelTulemus4 = new Label();
		Label labelTulemus5 = new Label();
		Label labelKaardid1 = new Label();
		Label labelKaardid2 = new Label();

		// Andmete sisestamine.
		TextField sisestusPanus = new TextField();
		TextField sisestusEnnustus = new TextField();

		// M2ngi nupp, mis kontrollib sisestatud andmete sobivust ja paneb
		// meetodi UksM2ng.m2ng() k2ima eeldusel, et vajalikud n6uded sisestuste
		// suhtes on t2idetud. Muul juhul n2itab pilti v�i annab veateate.
		// ActionEvent kood on v�etud siit:
		// http://www.java2s.com/Code/Java/JavaFX/ChangeLabeltextinButtonclickevent.htm
		nupp1 = new Button();
		nupp1.setText("M�ngi");
		nupp1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String panus = Meetodid.panus(Meetodid.stringSisestus(sisestusPanus));
				String ennustus = Meetodid.ennustus(Meetodid.stringSisestus(sisestusEnnustus));
				if (ennustus.equals("007") || panus.equals("007")) {
					aken.setScene(stseen3);
				} else if ((panus.equals("Vale sisestus")) || (ennustus.equals("Vale sisestus")) || (raha <= 0)
						|| (Integer.parseInt(panus) > raha)) {
					labelKaardid1.setText("Vigane sisestus!");
					labelKaardid2.setText("");
					System.out.println("Vigane sisestus!");
					labelTulemus1.setText("Vigane sisestus!");
					labelTulemus2.setText("");
					labelTulemus3.setText("");
					labelTulemus4.setText("");
					labelTulemus5.setText("");
					if (raha <= 0) {
						aken.close();
					}
				} else {
					tulemused = UksM2ng.m2ng(raha, Integer.parseInt(panus), ennustus);
					raha = Integer.parseInt(tulemused[4]);
					labelKaardid1.setText("M�ngija kaardid: " + tulemused[5]);
					labelKaardid2.setText("Panga kaardid: " + tulemused[6]);
					label1.setText("Raha j��k: " + Integer.toString(raha));
					labelTulemus1.setText("Sisestatud ennustus: " + tulemused[0]);
					labelTulemus2.setText("M�ngija tulemus: " + tulemused[1]);
					labelTulemus3.setText("Panga tulemus: " + tulemused[2]);
					labelTulemus4.setText("M�ngu v�itis: " + tulemused[3]);
					labelTulemus5.setText("Raha j��k: " + tulemused[4]);
				}
			}
		});
		nupp1.setStyle("-fx-font: 12 geneva; -fx-base: green;");

		// Tulemus nupp, avab uue akna, n�itab UksM2ng.m2ng() tulemusi.
		nupp2 = new Button("Tulemus");
		nupp2.setOnAction(
				e -> Tulemused.aken(labelTulemus1, labelTulemus2, labelTulemus3, labelTulemus4, labelTulemus5));
		nupp2.setStyle("-fx-font: 12 geneva; -fx-base: blue");

		// Tagasi nupp, mis muudab stseeni tagasi esimeseks, kasutatud pildi
		// stseeni juures.
		nupp4 = new Button("Tagasi");
		nupp4.setOnAction(e -> aken.setScene(stseen1));
		nupp4.setStyle("-fx-font: 12 geneva; -fx-base: purple;");

		// Nupp, mida kasutatakse pildi stseen juures ja muudab teise pildi
		// stseeniks.
		nupp5 = new Button("J�rgmine");
		nupp5.setOnAction(e -> aken.setScene(stseen4));
		nupp5.setStyle("-fx-font: 12 geneva; -fx-base: purple;");

		// Nupp, mida kasutatakse teise pildi stseeni juures ja muudab pildi
		// stseeniks.
		nupp6 = new Button("Eelmine");
		nupp6.setOnAction(e -> aken.setScene(stseen3));
		nupp6.setStyle("-fx-font: 12 geneva; -fx-base: purple;");

		// Esimese stseeni layout.
		GridPane layout1 = new GridPane();
		layout1.setPadding(new Insets(10, 10, 10, 10));
		layout1.setVgap(5);
		layout1.setVgap(5);
		GridPane.setConstraints(label1, 0, 0);
		GridPane.setConstraints(label2, 0, 2);
		GridPane.setConstraints(sisestusPanus, 0, 4);
		GridPane.setConstraints(label3, 0, 6);
		GridPane.setConstraints(sisestusEnnustus, 0, 8);
		GridPane.setConstraints(nupp1, 0, 10);
		GridPane.setConstraints(nupp2, 0, 12);
		GridPane.setConstraints(labelKaardid1, 0, 14);
		GridPane.setConstraints(labelKaardid2, 0, 16);
		layout1.getChildren().addAll(label1, label2, label3, nupp1, nupp2, sisestusPanus, sisestusEnnustus,
				labelKaardid1, labelKaardid2);
		stseen1 = new Scene(layout1, 300, 300);

		// Pildi stseeni layout.
		StackPane layout2 = new StackPane();
		Image pilt = new Image("bond.jpg");
		ImageView piltView = new ImageView(pilt);
		layout2.getChildren().addAll(piltView, nupp4, nupp5);
		StackPane.setAlignment(nupp5, Pos.TOP_RIGHT);
		StackPane.setAlignment(nupp4, Pos.TOP_LEFT);
		stseen3 = new Scene(layout2);

		// Teise pildi stseeni layout.
		StackPane layout3 = new StackPane();
		Image pilt2 = new Image("bond2.gif");
		ImageView pilt2View = new ImageView(pilt2);
		layout3.getChildren().addAll(pilt2View, nupp6);
		StackPane.setAlignment(nupp6, Pos.TOP_LEFT);
		stseen4 = new Scene(layout3);

		aken.setScene(stseen1);
		aken.show();

	}

	public static void main(String[] args) {
		launch(args);
	}

}

package kasutajaliides;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Tulemused {

	// Meetod tulemuste n2itamiseks.
	public static void aken(Label labelTulemus1, Label labelTulemus2, Label labelTulemus3, Label labelTulemus4,
			Label labelTulemus5) {
		
		Stage aken = new Stage();
		aken.initModality(Modality.APPLICATION_MODAL);
		aken.setTitle("Tulemused");

		// Nupp akna sulgemiseks.
		Button sulgeNupp = new Button("Tagasi");
		sulgeNupp.setOnAction(e -> aken.close());
		sulgeNupp.setStyle("-fx-font: 12 geneva; -fx-base: red;");

		// Stseeni layout.
		GridPane layout = new GridPane();
		layout.setPadding(new Insets(10, 10, 10, 10));
		layout.setVgap(5);
		layout.setVgap(5);
		GridPane.setConstraints(sulgeNupp, 0, 0);
		GridPane.setConstraints(labelTulemus1, 0, 2);
		GridPane.setConstraints(labelTulemus2, 0, 4);
		GridPane.setConstraints(labelTulemus3, 0, 6);
		GridPane.setConstraints(labelTulemus4, 0, 8);
		GridPane.setConstraints(labelTulemus5, 0, 10);
		layout.getChildren().addAll(sulgeNupp, labelTulemus1, labelTulemus2, labelTulemus3, labelTulemus4,
				labelTulemus5);
		Scene stseen = new Scene(layout, 250, 200);
		
		aken.setScene(stseen);
		aken.showAndWait();
	}
}
